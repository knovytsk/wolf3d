# wolf3d
My simple own version of Wolfenstein 3D.
Creation a 3D graphically “realistic” representation that we could have from
inside a maze in a subjective view. 

### skills ###
- ray-casting principles
- user events management
- textures application

### usage ###
- menu mode:

	./wolf3d 
	
- open a scene from file:

	./wolf3d [maps/map]
